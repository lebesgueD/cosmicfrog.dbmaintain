﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;

namespace CosmicFrog.DbMaintain.Messaging.Template
{
    public class GetTemplatesRequest : RequestBase
    {
        public string OwnerId { get; set; }
    }
}