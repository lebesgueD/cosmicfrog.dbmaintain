﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using CosmicFrog.DbMaintain.Messaging.Views;
using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Messaging.Template
{
    public class GetTemplatesResponse : ResponseBase<GetTemplatesRequest>
    {
        public IEnumerable<TemplatesListView> Templates { get; set; }
    }
}