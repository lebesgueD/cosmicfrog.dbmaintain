﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using CosmicFrog.DbMaintain.Messaging.Views;

namespace CosmicFrog.DbMaintain.Messaging.ModelDataset
{
    public class UpdateModelDatasetRequest : RequestBase
    {
        public ModelDatasetUpdateView ModelDataset { get; set; }

        public string OwnerId { get; set; }
    }
}