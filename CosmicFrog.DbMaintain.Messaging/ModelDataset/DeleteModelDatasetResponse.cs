﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;

namespace CosmicFrog.DbMaintain.Messaging.ModelDataset
{
    public class DeleteModelDatasetResponse : ResponseBase<DeleteModelDatasetRequest>
    { }
}