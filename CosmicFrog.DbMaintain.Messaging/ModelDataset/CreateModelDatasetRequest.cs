﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using CosmicFrog.DbMaintain.Messaging.Views;

namespace CosmicFrog.DbMaintain.Messaging.ModelDataset
{
    public class CreateModelDatasetRequest : RequestBase
    {
        public ModelDatasetCreateView NewModelDataset { get; set; }

        public string OwnerId { get; set; }
    }
}