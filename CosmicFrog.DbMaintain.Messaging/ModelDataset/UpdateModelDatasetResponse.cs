﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;

namespace CosmicFrog.DbMaintain.Messaging.ModelDataset
{
    public class UpdateModelDatasetResponse : ResponseBase<UpdateModelDatasetRequest>
    { }
}