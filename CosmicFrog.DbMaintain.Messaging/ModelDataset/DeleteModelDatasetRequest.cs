﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;

namespace CosmicFrog.DbMaintain.Messaging.ModelDataset
{
    public class DeleteModelDatasetRequest : RequestBase
    {
        public string OwnerId { get; set; }

        public int ModelId { get; set; }
    }
}