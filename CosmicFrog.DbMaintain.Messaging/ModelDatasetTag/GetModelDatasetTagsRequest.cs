﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;

namespace CosmicFrog.DbMaintain.Messaging.ModelDatasetTag
{
    public class GetModelDatasetTagsRequest : RequestBase
    {
        public string OwnerId { get; set; }

        public int ModelId { get; set; }
    }
}