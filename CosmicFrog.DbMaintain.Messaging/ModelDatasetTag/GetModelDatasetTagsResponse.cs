﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Messaging.ModelDatasetTag
{
    public class GetModelDatasetTagsResponse : ResponseBase<GetModelDatasetTagsRequest>
    {
        public IEnumerable<string> ModelDatasetTags { get; set; }
    }
}