﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using CosmicFrog.DbMaintain.Messaging.ModelDatasetTag;
using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Messaging.Views
{
    public class GetModelDatasetTagsResponseView : ResponseStatusViewBase
    {
        public IEnumerable<string> ModelDatasetTags { get; set; }

        public GetModelDatasetTagsResponseView(GetModelDatasetTagsResponse response)
        {
            Statuses = response.Statuses;
            ModelDatasetTags = response.ModelDatasetTags;
        }
    }
}