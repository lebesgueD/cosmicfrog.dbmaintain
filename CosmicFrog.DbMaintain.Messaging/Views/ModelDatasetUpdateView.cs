﻿using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Messaging.Views
{
    public class ModelDatasetUpdateView
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<Tag> Tags { get; set; }
    }
}