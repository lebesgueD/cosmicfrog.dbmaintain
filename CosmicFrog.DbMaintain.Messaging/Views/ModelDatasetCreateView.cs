﻿using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Messaging.Views
{
    public class ModelDatasetCreateView
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int ModelDatasetTypeId { get; set; }
        
        public List<Tag> Tags { get; set; }
    }

    public class Tag
    {
        public string Name { get; set; }
    }
}