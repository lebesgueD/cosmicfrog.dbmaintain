﻿using System;
using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Messaging.Views
{
    public class TemplatesListView
    {
        public int Id { get; set; }

        public string Uid { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ModelDatasetTypeView ModelDatasetType { get; set; }

        public List<ModelDatasetTagView> Tags { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }
    }
}