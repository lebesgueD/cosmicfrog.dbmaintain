﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using CosmicFrog.DbMaintain.Messaging.Template;
using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Messaging.Views
{
    public class GetTemplatesResponseView : ResponseStatusViewBase
    {
        public IEnumerable<TemplatesListView> Templates { get; set; }

        public GetTemplatesResponseView(GetTemplatesResponse response)
        {
            Statuses = response.Statuses;
            Templates = response.Templates;
        }
    }
}