﻿namespace CosmicFrog.DbMaintain.Messaging.Views
{
    public class ModelDatasetTagView
    {
        public string Name { get; set; }
    }
}