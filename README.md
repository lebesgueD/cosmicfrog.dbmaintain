# README #

This service maintains the relationship between users and models (including template models) in Cosmic Frog

Model details are stored in a SQL server table

# swagger endpoint
http://localhost:5000/swagger/index.html

# using the serverless database for the backend
While using the serverless database for dev environments should result in some cost savings, there are
a few things to note about using this from a client perspective. When the database goes idle, if your
client is not properly configured to account for the databases warm-up period you will get errors
telling you the database is not available. To that end when using these databases you need to make
sure the following things are accounted for in your connections  
* set the **ConnectionTimeout**, **ConnectRetryCount** and **ConnectRetryInterval** parameters per [this page](https://docs.microsoft.com/en-us/azure/azure-sql/database/troubleshoot-common-connectivity-issues) *Connection Timeout = ConnectRetryCount X ConnectionRetryInterval*
  * this is easy enough to set in the connection string
* You should set the commandTimeout as well, since if you happen to hit the server warming up there will be a period after it can connect that it takes to warm up before it will return results for queries.  
  `return db.Query<ModelDataset>(query, @params, commandTimeout: _defaultCommandTimeout);`
  * not all drivers support the *Command Timeout* parameter in the connection sting so it is likely safer to set this in the specific query you are running. Use the default in the connection string if you can, just know that it is not always available