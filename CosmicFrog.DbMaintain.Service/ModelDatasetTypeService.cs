﻿using AutoMapper;
using CosmicFrog.DbMaintain.Contract;
using CosmicFrog.DbMaintain.Infrastructure.Domain.Services;
using CosmicFrog.DbMaintain.Model.Repositories;
using Microsoft.Extensions.Logging;

namespace CosmicFrog.DbMaintain.Service
{
    public class ModelDatasetTypeService : DomainServiceBase, IModelDatasetTypeService
    {
        #region Fields

        private readonly ILogger<ModelDatasetService> _logger;
        private readonly IModelDatasetTypeRepository _modelDatasetTypeRepository;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public ModelDatasetTypeService
            (
            ILogger<ModelDatasetService> logger,
            IModelDatasetTypeRepository modelDatasetTypeRepository,
            IMapper mapper
            )
        {
            _logger = logger;
            _modelDatasetTypeRepository = modelDatasetTypeRepository;
            _mapper = mapper;
        }

        #endregion

        #region Public Methods
        #endregion
    }
}