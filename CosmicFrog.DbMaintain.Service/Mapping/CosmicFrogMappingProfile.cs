﻿using AutoMapper;
using CosmicFrog.DbMaintain.Messaging.Views;
using CosmicFrog.DbMaintain.Model.Domain;

namespace CosmicFrog.DbMaintain.Service.Mapping
{
    public class CosmicFrogMappingProfile : Profile
    {
        public CosmicFrogMappingProfile()
        {
            #region Model -> View
            
            CreateMap<ModelDataset, ModelDatasetCreateView>();
            CreateMap<ModelDataset, ModelDatasetUpdateView>();
            CreateMap<ModelDataset, TemplatesListView>();
            CreateMap<ModelDatasetType, ModelDatasetTypeView>();
            CreateMap<ModelDatasetTag, Tag>();
            CreateMap<ModelDatasetTag, ModelDatasetTagView>();

            #endregion

            #region View -> Model

            CreateMap<ModelDatasetCreateView, ModelDataset>();
            CreateMap<ModelDatasetUpdateView, ModelDataset>();
            CreateMap<TemplatesListView, ModelDataset>();
            CreateMap<ModelDatasetTypeView, ModelDatasetType>();
            CreateMap<Tag, ModelDatasetTag>();
            CreateMap<ModelDatasetTagView, ModelDatasetTag>();

            #endregion
        }
    }
}