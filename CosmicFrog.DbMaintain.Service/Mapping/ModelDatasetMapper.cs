﻿using AutoMapper;
using CosmicFrog.DbMaintain.Messaging.Views;
using CosmicFrog.DbMaintain.Model.Domain;
using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Service.Mapping
{
    public static class ModelDatasetMapper
    {
        #region Model -> View

        public static ModelDatasetCreateView MapToView(this ModelDataset model, IMapper mapper)
        {
            return mapper.Map<ModelDataset, ModelDatasetCreateView>(model);
        }

        public static IEnumerable<TemplatesListView> MapToListView(this IEnumerable<ModelDataset> model, IMapper mapper)
        {
            return mapper.Map<IEnumerable<ModelDataset>, IEnumerable<TemplatesListView>>(model);
        }

        #endregion

        #region View -> Model

        public static ModelDataset MapToModel(this ModelDatasetCreateView view, IMapper mapper)
        {
            return mapper.Map<ModelDatasetCreateView, ModelDataset>(view);
        }

        public static ModelDataset MapToModel(this ModelDatasetUpdateView view, IMapper mapper)
        {
            return mapper.Map<ModelDatasetUpdateView, ModelDataset>(view);
        }

        public static IEnumerable<ModelDataset> MapToModel(this IEnumerable<TemplatesListView> view, IMapper mapper)
        {
            return mapper.Map<IEnumerable<TemplatesListView>, IEnumerable<ModelDataset>>(view);
        }

        #endregion
    }
}