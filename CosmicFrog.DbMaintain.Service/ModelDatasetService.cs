﻿using AutoMapper;
using CosmicFrog.DbMaintain.Contract;
using CosmicFrog.DbMaintain.Infrastructure.Domain.Services;
using CosmicFrog.DbMaintain.Messaging.ModelDataset;
using CosmicFrog.DbMaintain.Messaging.ModelDatasetTag;
using CosmicFrog.DbMaintain.Messaging.Template;
using CosmicFrog.DbMaintain.Model.BusinessRules;
using CosmicFrog.DbMaintain.Model.Domain;
using CosmicFrog.DbMaintain.Model.Repositories;
using CosmicFrog.DbMaintain.Service.Mapping;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Service
{
    public class ModelDatasetService : DomainServiceBase, IModelDatasetService
    {
        #region Fields

        private readonly ILogger <ModelDatasetService> _logger;
        private readonly IModelDatasetRepository _modelDatasetRepository;
        private readonly IModelDatasetTagRepository _modelDatasetTagRepository;
        private readonly string _correlationId = Guid.NewGuid().ToString();
        private readonly IMapper _mapper;
        private readonly string _ip;

        #endregion

        #region Constructor

        public ModelDatasetService
            (
            ILogger<ModelDatasetService> logger,
            IModelDatasetRepository modelDatasetRepository,
            IModelDatasetTagRepository modelDatasetTagRepository,
            IMapper mapper,
            IHttpContextAccessor accessor
            )
        {
            _logger = logger;
            _modelDatasetRepository = modelDatasetRepository;
            _modelDatasetTagRepository = modelDatasetTagRepository;
            _mapper = mapper;
            _ip = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        #endregion

        #region Public Methods

        public async Task<GetTemplatesResponse> GetTemplates(GetTemplatesRequest request)
        {
            var response = new GetTemplatesResponse
            {
                Request = request,
                ResponseToken = Guid.NewGuid()
            };

            try
            {
                _logger.LogInformation("CorrelationId: {CorrelationId}, IP: {IP}. Proccessing GetTemplates", _correlationId, _ip);
                
                IEnumerable<ModelDataset> templates = await _modelDatasetRepository.GetDatasetsForOwner(request.OwnerId);
                
                response.Templates = templates.MapToListView(_mapper);

                response = FromBusinessRule<GetTemplatesRequest, GetTemplatesResponse>(response, ModelDatasetBusinessRules.ModelDatasetGetAllSuccess);
                response.Success = true;

                _logger.LogInformation("CorrelationId: {CorrelationId}, IP: {IP}. Responding with success...", _correlationId, _ip);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while getting templates. Exception: {@Exception}, CorrelationId: {CorrelationId}, IP: {IP}", ex.Message, _correlationId, _ip);
                response = GenericException<GetTemplatesRequest, GetTemplatesResponse>(response, ex);
            }

            return response;
        }

        public async Task<CreateModelDatasetResponse> CreateDataset(CreateModelDatasetRequest request)
        {
            var response = new CreateModelDatasetResponse
            {
                Request = request,
                ResponseToken = Guid.NewGuid()
            };

            try
            {
                ModelDataset newModelDataset = request.NewModelDataset.MapToModel(_mapper);
                newModelDataset.OwnerId = request.OwnerId;

                SetCreateTime(newModelDataset);

                if (string.IsNullOrEmpty(newModelDataset.OwnerId))
                    return FromBusinessRule<CreateModelDatasetRequest, CreateModelDatasetResponse>(response, ModelDatasetBusinessRules.CreateDatasetOwnerIdRequired);

                if (string.IsNullOrEmpty(newModelDataset.Name))
                    return FromBusinessRule<CreateModelDatasetRequest, CreateModelDatasetResponse>(response, ModelDatasetBusinessRules.DatasetNameRequired);

                if (newModelDataset.ModelDatasetTypeId < (int)Model.Enums.ModelDatasetType.Focussed || newModelDataset.ModelDatasetTypeId > (int)Model.Enums.ModelDatasetType.Advanced)
                    return FromBusinessRule<CreateModelDatasetRequest, CreateModelDatasetResponse>(response, ModelDatasetBusinessRules.WrongDataType);

                if (request.NewModelDataset.Tags.GroupBy(x => x.Name.ToLower()).Any(x => x.Count() > 1))
                    return FromBusinessRule<CreateModelDatasetRequest, CreateModelDatasetResponse>(response, ModelDatasetBusinessRules.DatasetTagDuplicate);

                IEnumerable<ModelDataset> usersDatasets = await _modelDatasetRepository.GetDatasetsForOwner(newModelDataset.OwnerId);
                if (usersDatasets.Any(x => x.Name == newModelDataset.Name))
                    return FromBusinessRule<CreateModelDatasetRequest, CreateModelDatasetResponse>(response, ModelDatasetBusinessRules.DatasetNameDuplicate);

                await _modelDatasetRepository.Add(newModelDataset);

                response = FromBusinessRule<CreateModelDatasetRequest, CreateModelDatasetResponse>(response, ModelDatasetBusinessRules.ModelDatasetCreateSuccess);
                response.Success = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while creating dataset. Exception: {@Exception}, CorrelationId: {CorrelationId}, IP: {IP}", ex.Message, _correlationId, _ip);
                response = GenericException<CreateModelDatasetRequest, CreateModelDatasetResponse>(response, ex);
            }

            return response;
        }

        public async Task<UpdateModelDatasetResponse> UpdateDataset(UpdateModelDatasetRequest request)
        {
            var response = new UpdateModelDatasetResponse
            {
                Request = request,
                ResponseToken = Guid.NewGuid()
            };

            try
            {
                if (request.ModelDataset.Id == 0)
                    return FromBusinessRule<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response, ModelDatasetBusinessRules.ModelIdRequired);

                if (string.IsNullOrEmpty(request.OwnerId))
                    return FromBusinessRule<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response, ModelDatasetBusinessRules.CreateDatasetOwnerIdRequired);

                if (string.IsNullOrEmpty(request.ModelDataset.Name))
                    return FromBusinessRule<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response, ModelDatasetBusinessRules.DatasetNameRequired);

                if (request.ModelDataset.Tags.GroupBy(x => x.Name.ToLower()).Any(x => x.Count() > 1))
                    return FromBusinessRule<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response, ModelDatasetBusinessRules.DatasetTagDuplicate);

                IEnumerable<ModelDataset> usersDatasets = await _modelDatasetRepository.GetDatasetsForOwner(request.OwnerId);
                if (usersDatasets.Where(x => x.Id != request.ModelDataset.Id).Any(x => x.Name == request.ModelDataset.Name))
                    return FromBusinessRule<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response, ModelDatasetBusinessRules.DatasetNameDuplicate);
                
                ModelDataset existingModelDataset = usersDatasets.FirstOrDefault(x => x.Id == request.ModelDataset.Id);
                if (existingModelDataset == null)
                    return ResourceNotFound<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response);

                existingModelDataset.Name = request.ModelDataset.Name;
                existingModelDataset.Description = request.ModelDataset.Description;
                existingModelDataset.Tags = request.ModelDataset.Tags.Select(x => new ModelDatasetTag() { Name = x.Name}).ToList();
                SetCreateTime(existingModelDataset);

                await _modelDatasetRepository.Update(existingModelDataset);

                response = FromBusinessRule<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response, ModelDatasetBusinessRules.ModelDatasetUpdateSuccess);
                response.Success = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while updating dataset. Exception: {@Exception}, CorrelationId: {CorrelationId}, IP: {IP}", ex.Message, _correlationId, _ip);
                response = GenericException<UpdateModelDatasetRequest, UpdateModelDatasetResponse>(response, ex);
            }

            return response;
        }

        public async Task<DeleteModelDatasetResponse> DeleteDataset(DeleteModelDatasetRequest request)
        {
            var response = new DeleteModelDatasetResponse
            {
                Request = request,
                ResponseToken = Guid.NewGuid()
            };

            try
            {
                if (request.ModelId <= 0)
                    return FromBusinessRule<DeleteModelDatasetRequest, DeleteModelDatasetResponse>(response, ModelDatasetBusinessRules.InvalidModelId);

                if (string.IsNullOrEmpty(request.OwnerId))
                    return FromBusinessRule<DeleteModelDatasetRequest, DeleteModelDatasetResponse>(response, ModelDatasetBusinessRules.DeleteDatasetOwnerIdRequired);

                ModelDataset existingModelDataset = await _modelDatasetRepository.GetDatasetByOwnerIdAndModelId(request.OwnerId, request.ModelId);
                if (existingModelDataset == null)
                    return ResourceNotFound<DeleteModelDatasetRequest, DeleteModelDatasetResponse>(response);

                DeleteExistingTags(existingModelDataset);

                await _modelDatasetRepository.Remove(existingModelDataset);

                response = FromBusinessRule<DeleteModelDatasetRequest, DeleteModelDatasetResponse>(response, ModelDatasetBusinessRules.ModelDatasetDeleteSuccess);
                response.Success = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while deleting dataset. Exception: {@Exception}, CorrelationId: {CorrelationId}, IP: {IP}", ex.Message, _correlationId, _ip);
                response = GenericException<DeleteModelDatasetRequest, DeleteModelDatasetResponse>(response, ex);
            }

            return response;
        }

        public  async Task<GetModelDatasetTagsResponse> GetModelDatasetTags(GetModelDatasetTagsRequest request)
        {
            var response = new GetModelDatasetTagsResponse
            {
                Request = request,
                ResponseToken = Guid.NewGuid()
            };

            try
            {
                if (request.ModelId <= 0)
                    return FromBusinessRule<GetModelDatasetTagsRequest, GetModelDatasetTagsResponse>(response, ModelDatasetBusinessRules.InvalidModelId);

                response.ModelDatasetTags = await _modelDatasetTagRepository.GetTagsByModelId(request.ModelId);

                response = FromBusinessRule<GetModelDatasetTagsRequest, GetModelDatasetTagsResponse>(response, ModelDatasetBusinessRules.ModelDatasetGetTagsSuccess);
                response.Success = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while getting dataset tags. Exception: {@Exception}, CorrelationId: {CorrelationId}, IP: {IP}", ex.Message, _correlationId, _ip);
                response = GenericException<GetModelDatasetTagsRequest, GetModelDatasetTagsResponse>(response, ex);
            }

            return response;
        }

        #endregion

        #region Private Methods

        private static ModelDataset SetCreateTime(ModelDataset modelDataset)
        {
            foreach (var tag in modelDataset.Tags)
            {
                tag.SetCreated();
            }

            return modelDataset;
        }

        private static ModelDataset DeleteExistingTags(ModelDataset modelDataset)
        {
            foreach (var tag in modelDataset.Tags.Where(x => x.IsDeleted == false))
            {
                tag.SetDeleted(true);
            }

            return modelDataset;
        }

        #endregion
    }
}