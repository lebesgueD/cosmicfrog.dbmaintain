FROM mcr.microsoft.com/dotnet/aspnet:5.0-focal AS base
WORKDIR /app
EXPOSE 5000
EXPOSE 5001
EXPOSE 2222


FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build
WORKDIR /src

COPY *.sln . 
COPY CosmicFrog.DbMaintain.Contract/*.csproj ./CosmicFrog.DbMaintain.Contract/
COPY CosmicFrog.DbMaintain.Infrastructure/*.csproj ./CosmicFrog.DbMaintain.Infrastructure/
COPY CosmicFrog.DbMaintain.IoC/*.csproj ./CosmicFrog.DbMaintain.IoC/
COPY CosmicFrog.DbMaintain.Messaging/*.csproj ./CosmicFrog.DbMaintain.Messaging/
COPY CosmicFrog.DbMaintain.Model/*.csproj ./CosmicFrog.DbMaintain.Model/
COPY CosmicFrog.DbMaintain.Repository/*.csproj ./CosmicFrog.DbMaintain.Repository/
COPY CosmicFrog.DbMaintain.Service/*.csproj ./CosmicFrog.DbMaintain.Service/
COPY CosmicFrog.DbMaintain.API/*.csproj ./CosmicFrog.DbMaintain.API/

RUN dotnet restore ./CosmicFrog.DbMaintain.API/CosmicFrog.DbMaintain.API.csproj
COPY . .
WORKDIR "/src/CosmicFrog.DbMaintain.API"
RUN dotnet build "CosmicFrog.DbMaintain.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "CosmicFrog.DbMaintain.API.csproj" -c Release -o /app/publish

FROM base AS final

# enable ssh
RUN apt update && apt install -y --no-install-recommends ssh \
     && echo "root:Docker!" | chpasswd 

# Copy the sshd_config file to the /etc/ssh/ directory
COPY sshd_config /etc/ssh/sshd_config

WORKDIR /app
COPY --from=publish /app/publish .

ENV ASPNETCORE_URLS=http://+:5000
#ENTRYPOINT ["dotnet", "CosmicFrog.DbMaintain.API.dll"]
ENTRYPOINT ["/bin/bash", "-c", "service ssh start && dotnet CosmicFrog.DbMaintain.API.dll"]