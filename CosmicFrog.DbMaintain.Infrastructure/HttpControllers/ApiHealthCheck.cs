﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Infrastructure.HttpControllers
{
    public class ApiHealthCheck : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context,
            CancellationToken cancellationToken = new CancellationToken())
        {
            //TODO: Implement logic here
            bool isHealthy = true;
            if (isHealthy)
                return Task.FromResult(HealthCheckResult.Healthy("I am one healthy API service"));

            return Task.FromResult(HealthCheckResult.Unhealthy("I am one unhealthy API service"));
        }

        public static Task WriteHealthCheckResponse(HttpContext httpContext, HealthReport result)
        {
            httpContext.Response.ContentType = "application/json";

            JObject jsonResponse = new (
                new JProperty("status", result.Status.ToString()),
                new JProperty("results", new JObject(result.Entries.Select(pair =>
                    new JProperty(pair.Key, new JObject(
                    new JProperty("totalDurationInSeconds", pair.Value.Duration),
                    new JProperty("status", pair.Value.Status.ToString()),
                    new JProperty("exception", pair.Value.Exception?.ToString()),
                    new JProperty("description", pair.Value.Description),
                    new JProperty("data", new JObject(pair.Value.Data.Select(
                        p => new JProperty(p.Key, p.Value))))))))
                )
            );

            return httpContext.Response.WriteAsync(jsonResponse.ToString(Formatting.Indented));
        }
    }
}