﻿using CosmicFrog.DbMaintain.Infrastructure.Helpers;
using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace CosmicFrog.DbMaintain.Infrastructure.HttpControllers
{
    public abstract class ApiControllerBase : ControllerBase
    {
        protected virtual string UserToken
        {
            get
            {
                Claim claim = null;

                if (User.Identity is ClaimsIdentity claimsIdentity)
                {
                    claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
                }

                var userToken = claim != null ? claim.Value : string.Empty;

                return userToken;
            }
        }

        protected virtual List<string> UserRoles
        {
            get
            {
                var userRoles = new List<string>();

                if (User.Identity is ClaimsIdentity claimsIdentity)
                {
                    var roleClaims = claimsIdentity.FindAll(ClaimTypes.Role);
                    userRoles.AddRange(roleClaims.Select(roleClaim => roleClaim.Value));
                }

                return userRoles;
            }
        }

        protected virtual Guid RequestToken => Request.Headers.TryGetValue("RequestToken", out var stringToken) ? new Guid(stringToken[0]) : Guid.NewGuid();

        protected virtual string AppUid => Request.Headers["AppUid"];

        public static string ExtractJwt(HttpRequest httpRequest) => httpRequest.Headers["Authorization"].ToString()
            .Replace("Bearer ", "");

        public static string ExtractImpersonateToken(HttpRequest httpRequest) => httpRequest.Headers["Impersonate"].ToString();

        public static string ExtractClaim(string token)
        {
            JwtSecurityTokenHandler handler = new();
            JwtSecurityToken jwtSecurityToken = handler.ReadToken(token) as JwtSecurityToken;

            return jwtSecurityToken.Claims.FirstOrDefault(x => x.Type == "sub").Value;
        }

        public static JObject DeserializeImpersonateToken(string impersonateToken)
        {
            string tokenJson = Base64Helper.Decode(impersonateToken);
            JObject tokenJObject = JObject.Parse(tokenJson);

            return tokenJObject;
        }

        protected string Culture
        {
            get
            {
                var language = "en";

                if (Request.Headers.TryGetValue("Language", out var languages))
                {
                    switch (languages[0])
                    {
                        case "0":
                            language = "hr";
                            break;
                        case "1":
                            language = "en";
                            break;
                        case "2":
                            language = "de";
                            break;
                        case "3":
                            language = "fr";
                            break;
                    }
                }

                return language;
            }
        }

        protected TRequest CreateServiceRequest<TRequest>()
            where TRequest : RequestBase, new()
        {
            return new TRequest
            {
                RequestToken = RequestToken,
                UserToken = UserToken,
                Culture = Culture,
                UserRoles = UserRoles,
                AppUid = AppUid,
            };
        }

        protected ObjectResult BadResponse<TRequest>(ResponseBase<TRequest> response, bool includeStatus = false)
            where TRequest : RequestBase, new()
        {
            return (response.Statuses.FirstOrDefault()?.Code) switch
            {
                "204" => StatusCode(StatusCodes.Status204NoContent, response.Message),
                "401" => StatusCode(StatusCodes.Status401Unauthorized, response.Message),
                "403" => StatusCode(StatusCodes.Status403Forbidden, response.Message),
                "404" => StatusCode(StatusCodes.Status404NotFound, response.Message),
                "500" => StatusCode(StatusCodes.Status500InternalServerError, response.Message),
                _ => includeStatus ? BadRequest(response.Statuses) : BadRequest(response.Message),
            };
        }
    }
}