﻿using System;

namespace CosmicFrog.DbMaintain.Infrastructure.Messaging
{
    [Serializable]
    public class ResponseStatus
    {
        public string Code { get; set; }

        public string Message { get; set; }
    }
}