﻿namespace CosmicFrog.DbMaintain.Infrastructure.Messaging
{
    public enum ResponseMessageCode
    {
        //200
        NoContent = 204,

        //400
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        Conflict = 409,
        Gone = 410,

        //500
        InternalServerError = 500
    }
}