﻿using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Infrastructure.Messaging
{
    public class ResponseStatusViewBase
    {
        public List<ResponseStatus> Statuses { get; set; }
    }
}