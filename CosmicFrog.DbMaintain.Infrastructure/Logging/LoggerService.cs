﻿using CosmicFrog.DbMaintain.Infrastructure.Configuration;
using Microsoft.Extensions.Configuration;
using NpgsqlTypes;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using Serilog.Sinks.MSSqlServer;
using Serilog.Sinks.PostgreSQL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace CosmicFrog.DbMaintain.Infrastructure.Logging
{
    public static class LoggerService
    {
        #region Fields

        private static readonly string ELASTICSEARCH_URI = string.Empty;
        
        #endregion

        #region Public Methods

        #region ElasticSearch

        public static void ConfigureElasticSearchLogger()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(
                    $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json",
                    optional: true)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(ConfigureElasticSink(environment))
                .Enrich.WithProperty("Environment", environment)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        #endregion

        #region MsSqlServer

        public static void ConfigureMsSqlServerLogger(LoggerDatabaseSettings loggerSettings)
        {
            Serilog.Debugging.SelfLog.Enable(msg =>
            {
                LoggerConfiguration loggerConfiguration = new LoggerConfiguration()
                    .MinimumLevel.Information()
                    .MinimumLevel.Override(nameof(Microsoft), LogEventLevel.Warning)
                    .MinimumLevel.Override(nameof(System), LogEventLevel.Warning);
                loggerConfiguration.WriteTo.Console();
                Log.Logger = loggerConfiguration.CreateLogger();
                Debug.WriteLine(msg);
                Console.WriteLine(msg);
            });

            var columnOptions = new Serilog.Sinks.MSSqlServer.ColumnOptions
            {
                DisableTriggers = true
            };
            columnOptions.Store.Remove(StandardColumn.MessageTemplate);
            columnOptions.Store.Remove(StandardColumn.Properties);

            var sinkOptions = new MSSqlServerSinkOptions
            {
                TableName = loggerSettings.TableName,
                SchemaName = loggerSettings.SchemaName,
                BatchPeriod = TimeSpan.FromSeconds(30),
                AutoCreateSqlTable = true
            };

            var loggerConfiguration = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override(nameof(Microsoft), LogEventLevel.Warning)
                .MinimumLevel.Override(nameof(System), LogEventLevel.Warning);

            if (string.IsNullOrWhiteSpace(loggerSettings.ConnectionString))
                loggerConfiguration.WriteTo.Console();
            else
                loggerConfiguration.WriteTo.MSSqlServer(
                    connectionString: loggerSettings.ConnectionString,
                    sinkOptions: sinkOptions,
                    columnOptions: columnOptions,
                    restrictedToMinimumLevel: LogEventLevel.Information
                );

            Log.Logger = loggerConfiguration.CreateLogger();
        }

        #endregion

        #region Postgre

        public static void ConfigurePostgreSQLLogger(string connectionString)
        {
            string connectionstring = connectionString;
            string tableName = "Logs";

            IDictionary<string, ColumnWriterBase> columnWriters = new Dictionary<string, ColumnWriterBase>
            {
                {"message", new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
                {"message_template", new MessageTemplateColumnWriter(NpgsqlDbType.Text) },
                {"level", new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
                {"raise_date", new TimestampColumnWriter(NpgsqlDbType.Timestamp) },
                {"exception", new ExceptionColumnWriter(NpgsqlDbType.Text) },
                {"properties", new LogEventSerializedColumnWriter(NpgsqlDbType.Jsonb) },
                {"props_test", new PropertiesColumnWriter(NpgsqlDbType.Jsonb) },
                {"machine_name", new SinglePropertyColumnWriter("MachineName", PropertyWriteMethod.ToString, NpgsqlDbType.Text, "l") }
            };

            using Serilog.Core.Logger logger = new LoggerConfiguration()
                                .WriteTo.PostgreSQL(connectionstring, tableName, columnWriters)
                                .CreateLogger();
        }

        #endregion

        #endregion

        #region Private Methods

        private static ElasticsearchSinkOptions ConfigureElasticSink(string environment) => new ElasticsearchSinkOptions(new Uri(ELASTICSEARCH_URI))
        {
            AutoRegisterTemplate = true,
            IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-")}-{environment?.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM-dd}"
        };

        #endregion
    }
}