﻿using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    public interface IRepositoryAsNoTracking<T> : IRepository<T> where T : class
    {
        Task<T> AddAsNoTracking(T entity);

        Task<T> UpdateAsNoTracking(T entity);
    }
}