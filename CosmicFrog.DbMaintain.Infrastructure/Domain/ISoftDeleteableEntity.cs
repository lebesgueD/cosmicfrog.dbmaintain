﻿namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    public interface ISoftDeleteableEntity
    {
        bool IsDeleted { get; }

        void SetDeleted(bool deleted);
    }
}