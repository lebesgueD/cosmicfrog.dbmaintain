﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    public interface IRepository<T> where T : class
    {
        Task<T> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeExpressions);

        Task<IEnumerable<T>> FindAll(params Expression<Func<T, object>>[] includeExpressions);

        Task<IEnumerable<T>> FindAllBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeExpressions);

        Task<T> Add(T entity);

        Task<IEnumerable<T>> AddRange(IEnumerable<T> entities);

        Task<T> Remove(T entity);

        Task<IEnumerable<T>> RemoveRange(IEnumerable<T> entities);

        Task<T> Update(T entity);

        Task<int> CountAllBy(Expression<Func<T, bool>> predicate, Expression<Func<T, bool>> includePredicate, params Expression<Func<T, object>>[] includeExpressions);

        Task<int> CountAllBy(Expression<Func<T, bool>>[] predicateExpressions);

        Task<int> CountAllBy(Expression<Func<T, bool>> predicate);

        Task<int> CountAll();
    }
}