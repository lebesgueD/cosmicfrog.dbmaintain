﻿using CosmicFrog.DbMaintain.Infrastructure.Messaging;
using System;

namespace CosmicFrog.DbMaintain.Infrastructure.Domain.Services
{
    public abstract class DomainServiceBase
    {
        #region Fields

        private static readonly Rule _contentNotFound = new Rule("204")
            .AddCulturedRule(Culture.En, "Content not found.");

        private static readonly Rule _userUnauthenticated = new Rule("401")
            .AddCulturedRule(Culture.En, "User is not authenticated.");

        private static readonly Rule _userUnauthorized = new Rule("403")
            .AddCulturedRule(Culture.En, "User is not authorized for the requested action.");

        private static readonly Rule _resourceNotFound = new Rule("404")
            .AddCulturedRule(Culture.En, "Resource not found.");

        private static readonly Rule _genericException = new Rule("500")
            .AddCulturedRule(Culture.En, "Internal server error.");

        #endregion

        #region Protected Methods

        protected static TResponse ContentNotFound<TRequest, TResponse>(TResponse response)
            where TRequest : RequestBase
            where TResponse : ResponseBase<TRequest>
        {
            return FromBusinessRule<TRequest, TResponse>(response, _contentNotFound);
        }

        protected static TResponse UserUnauthenticated<TRequest, TResponse>(TResponse response)
            where TRequest : RequestBase
            where TResponse : ResponseBase<TRequest>
        {
            return FromBusinessRule<TRequest, TResponse>(response, _userUnauthenticated);
        }

        protected static TResponse UserUnauthorized<TRequest, TResponse>(TResponse response)
            where TRequest : RequestBase
            where TResponse : ResponseBase<TRequest>
        {
            return FromBusinessRule<TRequest, TResponse>(response, _userUnauthorized);
        }

        protected static TResponse ResourceNotFound<TRequest, TResponse>(TResponse response)
            where TRequest : RequestBase
            where TResponse : ResponseBase<TRequest>
        {
            return FromBusinessRule<TRequest, TResponse>(response, _resourceNotFound);
        }

        protected static TResponse GenericException<TRequest, TResponse>(TResponse response, Exception exception)
            where TRequest : RequestBase
            where TResponse : ResponseBase<TRequest>
        {
            response = FromBusinessRule<TRequest, TResponse>(response, _genericException);
            response.Exception = exception;

            return response;
        }

        protected static TResponse FromBusinessRule<TRequest, TResponse>(TResponse response, Rule businessRule)
            where TRequest : RequestBase
            where TResponse : ResponseBase<TRequest>
        {
            if (businessRule != null)
            {
                var message = businessRule.GetRuleFor(response.Request.Culture.ToCultureEnum());

                response.Message = message;
                response.Statuses.Add
                    (
                        new ResponseStatus
                        {
                            Code = businessRule.GetCode(),
                            Message = message
                        }
                    );
            }
            else
            {
                response.Message = string.Empty;
            }

            response.Success = false;

            return response;
        }

        #endregion
    }
}