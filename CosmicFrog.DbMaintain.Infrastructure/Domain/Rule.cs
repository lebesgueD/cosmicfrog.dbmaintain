﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    [Serializable]
    public class Rule
    {
        private readonly string _code;
        private readonly IList<CulturedRule> _culturedRules;

        public Rule()
        {
            _culturedRules = new List<CulturedRule>();
        }

        public Rule(string code)
            : this()
        {
            _code = code;
        }

        public Rule AddCulturedRule(Culture culture, string value)
        {
            var culturedRule = new CulturedRule
            {
                Culture = culture,
                Value = value
            };

            _culturedRules.Add(culturedRule);

            return this;
        }

        public string GetRuleFor(Culture culture = Culture.Hr)
        {
            if (_culturedRules.Count == 0)
                return _code;

            var matchedCulturedRule = _culturedRules.FirstOrDefault(culturedRule => culturedRule.Culture == culture);

            if (matchedCulturedRule != null)
            {
                return matchedCulturedRule.Value;
            }

            return _code;
        }

        public string GetCode()
        {
            return _code;
        }
    }
}