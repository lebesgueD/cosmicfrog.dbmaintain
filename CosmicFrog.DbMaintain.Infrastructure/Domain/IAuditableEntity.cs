﻿using System;

namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    public interface IAuditableEntity
    {
        DateTime CreatedAt { get; }

        DateTime? UpdatedAt { get; }

        void SetCreated();

        void SetUpdated();
    }
}