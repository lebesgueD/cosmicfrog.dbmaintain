﻿namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    public class CulturedRule
    {
        public Culture Culture { get; set; }

        public string Value { get; set; }
    }
}