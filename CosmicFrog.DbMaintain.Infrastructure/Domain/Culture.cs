﻿namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    public enum Culture
    {
        Hr = 0,
        En = 1,
        De = 2,
        Fr = 3
    }
}