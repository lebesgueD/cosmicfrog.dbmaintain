﻿namespace CosmicFrog.DbMaintain.Infrastructure.Domain
{
    public static class CultureConverter
    {
        public static Culture ToCultureEnum(this string cultureCode)
        {
            if (string.IsNullOrWhiteSpace(cultureCode))
            {
                return Culture.Hr;
            }

            var code = cultureCode.ToLower();
            return code switch
            {
                "en" => Culture.En,
                "de" => Culture.De,
                "fr" => Culture.Fr,
                _ => Culture.Hr,
            };
        }

        public static string FromCultureEnum(this Culture culture)
        {
            return culture switch
            {
                Culture.En => "en",
                Culture.De => "de",
                Culture.Fr => "fr",
                _ => "hr",
            };
        }
    }
}