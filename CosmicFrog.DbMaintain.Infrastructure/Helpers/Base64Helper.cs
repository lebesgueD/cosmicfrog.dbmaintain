﻿using System;
using System.Text;

namespace CosmicFrog.DbMaintain.Infrastructure.Helpers
{
	public static class Base64Helper
	{
		public static string Encode(string plainText)
		{
			byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			return Convert.ToBase64String(plainTextBytes);
		}

		public static string Decode(string base64EncodedData)
		{
			byte[] base64EncodedBytes = Convert.FromBase64String(Repad(base64EncodedData));
			return Encoding.UTF8.GetString(base64EncodedBytes);
		}

		private static string Repad(string base64)
		{
            switch (base64.Length % 4) // Pad with trailing '='s
            {
				case 0: break; // No pad chars in this case
				case 2: base64 += "=="; break; // Two pad chars
				case 3: base64 += "="; break; // One pad char
				default:
					throw new Exception("Illegal base64url string!");
			}

			return base64;
		}
	}
}