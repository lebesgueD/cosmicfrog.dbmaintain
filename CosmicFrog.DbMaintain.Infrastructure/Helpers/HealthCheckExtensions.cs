﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using System.Linq;

namespace CosmicFrog.DbMaintain.Infrastructure.Helpers
{
    public static class HealthCheckExtensions
    {
        public static void MapHealthChecksWithJsonResponse(this IEndpointRouteBuilder endpoints, PathString path)
        {
            var options = new HealthCheckOptions
            {
                ResponseWriter = async (httpContext, healthReport) =>
                {
                    httpContext.Response.ContentType = "application/json";

                    string result = JsonConvert.SerializeObject(new
                    {
                        status = healthReport.Status.ToString(),
                        totalDurationInSeconds = healthReport.TotalDuration.TotalSeconds,
                        entries = healthReport.Entries.Select(e => new
                        {
                            key = e.Key,
                            status = e.Value.Status.ToString(),
                            description = e.Value.Description,
                            data = e.Value.Data,
                            exception = e.Value.Exception?.ToString()
                        })
                    });

                    await httpContext.Response.WriteAsync(result);
                }
            };

            endpoints.MapHealthChecks(path, options);
        }
    }
}