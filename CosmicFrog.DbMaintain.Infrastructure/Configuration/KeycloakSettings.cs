﻿namespace CosmicFrog.DbMaintain.Infrastructure.Configuration
{
	public class KeycloakSettings
	{
		public string Authority { get; set; }

		public string Endpoint { get; set; }

		public string Realm { get; set; }

		public string ClientId { get; set; }

		public string Secret { get; set; }

		public bool RequireHttpsMetadata { get; set; }

		public bool SaveTokens { get; set; }

		public bool GetClaimsFromUserInfoEndpoint { get; set; }

		public bool IncludeErrorDetails { get; set; }

		public bool ValidateAudience { get; set; }

		public bool ValidateIssuerSigningKey { get; set; }

		public bool ValidateIssuer { get; set; }

		public bool ValidateLifetime { get; set; }
	}
}