﻿namespace CosmicFrog.DbMaintain.Infrastructure.Configuration
{
    public class StatsD
    {
        public string Server { get; set; }

        public int Port { get; set; }
    }
}