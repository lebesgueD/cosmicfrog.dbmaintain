﻿namespace CosmicFrog.DbMaintain.Infrastructure.Configuration
{
    public class DatabaseConfig
    {
        public string ConnectionString { get; set; }
    }
}