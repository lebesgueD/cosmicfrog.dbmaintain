﻿namespace CosmicFrog.DbMaintain.Infrastructure.Configuration
{
    public class LoggerDatabaseSettings : DatabaseConfig
    {
        public string TableName { get; set; }

        public string SchemaName { get; set; }

        public int BatchPeriod { get; set; }
    }
}