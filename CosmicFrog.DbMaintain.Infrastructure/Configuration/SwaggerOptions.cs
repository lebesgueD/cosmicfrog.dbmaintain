﻿namespace CosmicFrog.DbMaintain.Infrastructure.Configuration
{
    public class SwaggerOptions
    {
        public string JsonRoute { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public string UiEndpoint { get; set; } = string.Empty;

        public string RoutePrefix { get; set; } = string.Empty;
    }
}