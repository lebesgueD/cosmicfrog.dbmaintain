﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Infrastructure.Repository
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class, IAuditableEntity
    {
        #region Fields

        private readonly DbContext _dbContext;
        private readonly DbSet<T> _entity;

        #endregion

        #region Constructor

        public RepositoryBase(DbContext dbContext)
        {
            _dbContext = dbContext;
            _entity = dbContext.Set<T>();
        }

        #endregion

        #region Public Methods

        public virtual async Task<T> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(predicate);

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
                query = query.Where(x => ((ISoftDeleteableEntity)x).IsDeleted == false);

            foreach (var includeExpression in includeExpressions)
            {
                query = query.Include(includeExpression);
            }

            return await query.FirstOrDefaultAsync();
        }

        public virtual async Task<IEnumerable<T>> FindAll(params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> query = _dbContext.Set<T>();

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
            {
                query = query.Where(x => ((ISoftDeleteableEntity)x).IsDeleted == false);
            }

            foreach (var includeExpression in includeExpressions)
            {
                query = query.Include(includeExpression);
            }

            return await query.ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> FindAllBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(predicate);

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
                query = query.Where(x => ((ISoftDeleteableEntity)x).IsDeleted == false);

            foreach (var includeExpression in includeExpressions)
            {
                query = query.Include(includeExpression);
            }

            return await query.ToListAsync();
        }

        public virtual async Task<T> Add(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);

            if (entity is IAuditableEntity)
                entity.SetCreated();

            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<IEnumerable<T>> AddRange(IEnumerable<T> entities)
        {
            var entitiesList = entities.ToList();

            if (typeof(IAuditableEntity).IsAssignableFrom(typeof(T)))
                entitiesList.ForEach(x => x.SetCreated());

            await _dbContext.Set<T>().AddRangeAsync(entitiesList);
            await _dbContext.SaveChangesAsync();

            return entitiesList;
        }

        public virtual async Task<T> Update(T entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbContext.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                _dbContext.Entry(entity).CurrentValues.SetValues(entity);
            }

            if (entity is IAuditableEntity)
            {
                entity.SetUpdated();
            }

            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<T> Remove(T entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached)
                _dbContext.Attach(entity);

            if (entity is ISoftDeleteableEntity entity1)
                entity1.SetDeleted(true);
            else
                _dbContext.Set<T>().Remove(entity);

            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<IEnumerable<T>> RemoveRange(IEnumerable<T> entities)
        {
            var entitiesList = entities.ToList();

            foreach (var entity in entitiesList)
            {
                if (_dbContext.Entry(entity).State == EntityState.Detached)
                    _dbContext.Attach(entity);
            }

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
            {
                entitiesList.ForEach(x => ((ISoftDeleteableEntity)x).SetDeleted(true));
            }
            else
            {
                _dbContext.Set<T>().RemoveRange(entitiesList);
            }

            await _dbContext.SaveChangesAsync();

            return entitiesList;
        }

        public virtual async Task<int> CountAllBy(Expression<Func<T, bool>> predicate, Expression<Func<T, bool>> includePredicate, params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(predicate);

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
            {
                query = query.Where(x => ((ISoftDeleteableEntity)x).IsDeleted == false);
            }

            foreach (var includeExpression in includeExpressions)
            {
                query = query.Include(includeExpression);
            }

            query = query.Where(includePredicate);

            return await query.CountAsync();
        }

        public virtual async Task<int> CountAllBy(Expression<Func<T, bool>>[] predicateExpressions)
        {
            IQueryable<T> query = _dbContext.Set<T>();

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
                query = query.Where(x => ((ISoftDeleteableEntity)x).IsDeleted == false);

            foreach (var predicateExpression in predicateExpressions)
            {
                query = query.Where(predicateExpression);
            }

            return await query.CountAsync();
        }

        public virtual async Task<int> CountAllBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(predicate);

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
                query = query.Where(x => ((ISoftDeleteableEntity)x).IsDeleted == false);

            return await query.CountAsync();
        }

        public virtual async Task<int> CountAll()
        {
            IQueryable<T> query = _dbContext.Set<T>();

            if (typeof(ISoftDeleteableEntity).IsAssignableFrom(typeof(T)))
                query = query.Where(x => ((ISoftDeleteableEntity)x).IsDeleted == false);

            return await query.CountAsync();
        }

        #region AsNoTracking

        public virtual async Task<T> AddAsNoTracking(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);

            if (entity is IAuditableEntity)
                entity.SetCreated();

            await _dbContext.SaveChangesAsync();

            _dbContext.Entry(entity).State = EntityState.Detached;

            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<T> UpdateAsNoTracking(T entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbContext.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                _dbContext.Entry(entity).CurrentValues.SetValues(entity);
            }

            if (entity is IAuditableEntity)
                entity.SetUpdated();

            await _dbContext.SaveChangesAsync();

            _dbContext.Entry(entity).State = EntityState.Detached;

            await _dbContext.SaveChangesAsync();

            return entity;
        }

        #endregion

        #endregion
    }
}