﻿using CosmicFrog.DbMaintain.Messaging.ModelDataset;
using CosmicFrog.DbMaintain.Messaging.ModelDatasetTag;
using CosmicFrog.DbMaintain.Messaging.Template;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Contract
{
    public interface IModelDatasetService
    {
        Task<GetTemplatesResponse> GetTemplates(GetTemplatesRequest request);

        Task<CreateModelDatasetResponse> CreateDataset(CreateModelDatasetRequest request);

        Task<UpdateModelDatasetResponse> UpdateDataset(UpdateModelDatasetRequest request);

        Task<DeleteModelDatasetResponse> DeleteDataset(DeleteModelDatasetRequest request);

        Task<GetModelDatasetTagsResponse> GetModelDatasetTags(GetModelDatasetTagsRequest request);
    }
}