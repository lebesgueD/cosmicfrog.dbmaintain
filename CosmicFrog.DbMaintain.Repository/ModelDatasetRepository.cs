﻿using CosmicFrog.DbMaintain.Infrastructure.Repository;
using CosmicFrog.DbMaintain.Model.Domain;
using CosmicFrog.DbMaintain.Model.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Repository
{
    public class ModelDatasetRepository : RepositoryBase<ModelDataset>, IModelDatasetRepository
    {
        #region Fields

        private readonly DataContext _dataContext;

        #endregion

        #region Constructor

        public ModelDatasetRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        #endregion

        #region Public Methods

        public async Task<ModelDataset> GetModelDataset(int id) => await _dataContext.Set<ModelDataset>()
            .Where(x => x.Id == id)
            .Where(x => x.IsActive == true)
            .Include(x => x.ModelDatasetType)
            .Include(x => x.Tags
                .Where(x => x.IsActive == true))
            .AsNoTracking()
            .FirstOrDefaultAsync();

        public async Task<IEnumerable<ModelDataset>> GetDatasetsForOwner(string ownerId) => await _dataContext.Set<ModelDataset>()
            .Where(x => x.OwnerId == ownerId)
            .Where(x => x.IsActive == true)
            .Include(x => x.ModelDatasetType)
            .Include(x => x.Tags
                .Where(x => x.IsActive == true))
            .ToListAsync();

        public async Task<ModelDataset> GetDatasetByOwnerIdAndModelId(string ownerId, int modelId) => await _dataContext.Set<ModelDataset>()
            .Where(x => x.OwnerId == ownerId)
            .Where(x => x.IsActive == true)
            .Where(x => x.Id == modelId)
            .Include(x => x.ModelDatasetType)
            .Include(x => x.Tags
                .Where(x => x.IsActive == true))
            .FirstOrDefaultAsync();

        #endregion
    }
}