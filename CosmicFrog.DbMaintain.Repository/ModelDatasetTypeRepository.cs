﻿using CosmicFrog.DbMaintain.Infrastructure.Repository;
using CosmicFrog.DbMaintain.Model.Domain;
using CosmicFrog.DbMaintain.Model.Repositories;

namespace CosmicFrog.DbMaintain.Repository
{
    public class ModelDatasetTypeRepository : RepositoryBase<ModelDatasetType>, IModelDatasetTypeRepository
    {
        #region Fields

        private readonly DataContext _dataContext;

        #endregion

        #region Constructor

        public ModelDatasetTypeRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        #endregion

        #region Public Methods
        #endregion
    }
}