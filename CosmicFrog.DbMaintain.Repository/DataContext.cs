﻿using CosmicFrog.DbMaintain.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace CosmicFrog.DbMaintain.Repository
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        #region DbSets
        
        public DbSet<ModelDataset> ModelDataset { get; set; }

        public DbSet<ModelDatasetType> ModelDatasetType { get; set; }

        public DbSet<ModelDatasetTag> ModelDatasetTag { get; set; }

        public DbSet<SharedDataset> SharedDataset { get; set; }

        public DbSet<Folder> Folder { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { }
    }
}