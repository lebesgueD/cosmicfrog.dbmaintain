﻿using CosmicFrog.DbMaintain.Infrastructure.Repository;
using CosmicFrog.DbMaintain.Model.Domain;
using CosmicFrog.DbMaintain.Model.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Repository
{
    public class ModelDatasetTagRepository : RepositoryBase<ModelDatasetTag>, IModelDatasetTagRepository
    {
        #region Fields

        private readonly DataContext _dataContext;

        #endregion

        #region Constructor

        public ModelDatasetTagRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        #endregion

        #region Public Methods

        public async Task<IEnumerable<string>> GetTagsByModelId(int modelId) => await _dataContext.Set<ModelDatasetTag>()
            .Where(x => x.ModelDatasetId == modelId)
            .Where(x => x.IsActive == true)
            .Select(x => x.Name)
            .ToListAsync();

        #endregion
    }
}