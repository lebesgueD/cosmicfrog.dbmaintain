﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CosmicFrog.DbMaintain.Repository.Migrations
{
    public partial class SharedDataset : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SharedDatasetId",
                table: "ModelDataset",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "SharedDataset",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Uid = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModelDatasetId = table.Column<int>(type: "int", nullable: false),
                    SharerId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShareeEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SharedDataset", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SharedDataset_ModelDataset_ModelDatasetId",
                        column: x => x.ModelDatasetId,
                        principalTable: "ModelDataset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SharedDataset_ModelDatasetId",
                table: "SharedDataset",
                column: "ModelDatasetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SharedDataset");

            migrationBuilder.DropColumn(
                name: "SharedDatasetId",
                table: "ModelDataset");
        }
    }
}
