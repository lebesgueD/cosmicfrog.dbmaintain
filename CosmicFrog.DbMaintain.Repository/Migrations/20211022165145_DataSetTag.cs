﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CosmicFrog.DbMaintain.Repository.Migrations
{
    public partial class DataSetTag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ModelDatasetTag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Uid = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModelDatasetId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelDatasetTag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModelDatasetTag_ModelDataset_ModelDatasetId",
                        column: x => x.ModelDatasetId,
                        principalTable: "ModelDataset",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ModelDatasetTag_ModelDatasetId",
                table: "ModelDatasetTag",
                column: "ModelDatasetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModelDatasetTag");
        }
    }
}
