﻿// <auto-generated />
using System;
using CosmicFrog.DbMaintain.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CosmicFrog.DbMaintain.Repository.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20211022165145_DataSetTag")]
    partial class DataSetTag
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.10")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CosmicFrog.DbMaintain.Model.Domain.ModelDataset", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<int>("ModelDatasetTypeId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("OwnerId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Uid")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("ModelDatasetTypeId");

                    b.ToTable("ModelDataset");
                });

            modelBuilder.Entity("CosmicFrog.DbMaintain.Model.Domain.ModelDatasetTag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<int>("ModelDatasetId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Uid")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("ModelDatasetId");

                    b.ToTable("ModelDatasetTag");
                });

            modelBuilder.Entity("CosmicFrog.DbMaintain.Model.Domain.ModelDatasetType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Uid")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("ModelDatasetType");
                });

            modelBuilder.Entity("CosmicFrog.DbMaintain.Model.Domain.ModelDataset", b =>
                {
                    b.HasOne("CosmicFrog.DbMaintain.Model.Domain.ModelDatasetType", "ModelDatasetType")
                        .WithMany("ModelDatasets")
                        .HasForeignKey("ModelDatasetTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ModelDatasetType");
                });

            modelBuilder.Entity("CosmicFrog.DbMaintain.Model.Domain.ModelDatasetTag", b =>
                {
                    b.HasOne("CosmicFrog.DbMaintain.Model.Domain.ModelDataset", "ModelDataset")
                        .WithMany("ModelDatasetTags")
                        .HasForeignKey("ModelDatasetId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ModelDataset");
                });

            modelBuilder.Entity("CosmicFrog.DbMaintain.Model.Domain.ModelDataset", b =>
                {
                    b.Navigation("ModelDatasetTags");
                });

            modelBuilder.Entity("CosmicFrog.DbMaintain.Model.Domain.ModelDatasetType", b =>
                {
                    b.Navigation("ModelDatasets");
                });
#pragma warning restore 612, 618
        }
    }
}
