﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CosmicFrog.DbMaintain.Repository.Migrations
{
    public partial class FolderUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserFirstName",
                table: "Folder",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserLastName",
                table: "Folder",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserFirstName",
                table: "Folder");

            migrationBuilder.DropColumn(
                name: "UserLastName",
                table: "Folder");
        }
    }
}
