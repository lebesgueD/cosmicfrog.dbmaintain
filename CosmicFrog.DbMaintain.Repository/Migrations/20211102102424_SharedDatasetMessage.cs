﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CosmicFrog.DbMaintain.Repository.Migrations
{
    public partial class SharedDatasetMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Message",
                table: "SharedDataset",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Message",
                table: "SharedDataset");
        }
    }
}
