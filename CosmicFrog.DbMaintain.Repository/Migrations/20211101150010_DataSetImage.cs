﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CosmicFrog.DbMaintain.Repository.Migrations
{
    public partial class DataSetImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "ModelDataset",
                type: "varbinary(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "ModelDataset");
        }
    }
}
