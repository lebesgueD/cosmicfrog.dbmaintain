﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;
using CosmicFrog.DbMaintain.Model.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Model.Repositories
{
    public interface IModelDatasetRepository : IRepository<ModelDataset>, IRepositoryAsNoTracking<ModelDataset>
    {
        Task<ModelDataset> GetModelDataset(int id);

        Task<IEnumerable<ModelDataset>> GetDatasetsForOwner(string ownerId);

        Task<ModelDataset> GetDatasetByOwnerIdAndModelId(string ownerId, int modelId);
    }
}