﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;
using CosmicFrog.DbMaintain.Model.Domain;

namespace CosmicFrog.DbMaintain.Model.Repositories
{
    public interface IModelDatasetTypeRepository : IRepository<ModelDatasetType>, IRepositoryAsNoTracking<ModelDatasetType>
    { }
}