﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;
using CosmicFrog.DbMaintain.Model.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.Model.Repositories
{
    public interface IModelDatasetTagRepository : IRepository<ModelDatasetTag>, IRepositoryAsNoTracking<ModelDatasetTag>
    {
        Task<IEnumerable<string>> GetTagsByModelId(int modelId);
    }
}