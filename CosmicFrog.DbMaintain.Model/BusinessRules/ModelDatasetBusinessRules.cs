﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;

namespace CosmicFrog.DbMaintain.Model.BusinessRules
{
    public static class ModelDatasetBusinessRules
    {
        #region Success

        public static Rule ModelDatasetGetAllSuccess => new Rule("200")
            .AddCulturedRule(Culture.En, "Successfully aquired all the Templates");

       public static Rule ModelDatasetCreateSuccess => new Rule("200")
            .AddCulturedRule(Culture.En, "Dataset successfully created");

        public static Rule ModelDatasetUpdateSuccess => new Rule("200")
            .AddCulturedRule(Culture.En, "Dataset successfully updated");

        public static Rule ModelDatasetDeleteSuccess => new Rule("200")
            .AddCulturedRule(Culture.En, "Dataset successfully deleted");

        public static Rule ModelDatasetGetTagsSuccess => new Rule("200")
            .AddCulturedRule(Culture.En, "Successfully aquired all the dataset's tags");

        #endregion

        #region Bad Request

        public static Rule TemplateDoesNotExists(int id) => new Rule("400")
            .AddCulturedRule(Culture.En, $"Template with Id {id} does not exists");

        public static Rule NullDataset => new Rule("400")
            .AddCulturedRule(Culture.En, "Cannot pass null dataset");

        public static Rule CreateDatasetOwnerIdRequired => new Rule("400")
            .AddCulturedRule(Culture.En, "Cannot create without owner");

        public static Rule DeleteDatasetOwnerIdRequired => new Rule("400")
            .AddCulturedRule(Culture.En, "Cannot delete without owner");

        public static Rule WrongDataType => new Rule("400")
            .AddCulturedRule(Culture.En, "Wrong dataset type id");

        public static Rule DatasetNameRequired => new Rule("400")
            .AddCulturedRule(Culture.En, "Cannot create without name");

        public static Rule ModelIdRequired => new Rule("400")
            .AddCulturedRule(Culture.En, "Cannot update without model id");

        public static Rule InvalidModelId => new Rule("400")
            .AddCulturedRule(Culture.En, "Cannot pass invalid model id");

        public static Rule DatasetNameDuplicate => new Rule("400")
            .AddCulturedRule(Culture.En, "Dataset name must be unique per user");

        public static Rule DatasetTagDuplicate => new Rule("400")
            .AddCulturedRule(Culture.En, "Dataset tag must be unique per dataset");

        #endregion
    }
}