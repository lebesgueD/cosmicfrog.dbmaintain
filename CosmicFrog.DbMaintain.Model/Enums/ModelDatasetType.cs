﻿namespace CosmicFrog.DbMaintain.Model.Enums
{
    public enum ModelDatasetType
    {
        Focussed = 1,
        Advanced = 2
    }
}