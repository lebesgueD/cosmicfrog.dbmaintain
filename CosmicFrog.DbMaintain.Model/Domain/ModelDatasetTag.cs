﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;
using System;

namespace CosmicFrog.DbMaintain.Model.Domain
{
    public class ModelDatasetTag : IAuditableEntity, ISoftDeleteableEntity
    {
        public int Id { get; set; }

        public string Uid { get; set; }

        public string Name { get; set; }

        public ModelDataset ModelDataset { get; set; }
        
        public int ModelDatasetId { get; set; }
        
        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public ModelDatasetTag()
        {
            IsActive = true;
            Uid = Guid.NewGuid().ToString();
        }

        public void SetCreated()
        {
            CreatedAt = DateTime.Now;
        }

        public void SetUpdated()
        {
            UpdatedAt = DateTime.Now;
        }

        public void SetActive(bool isActive)
        {
            IsActive = isActive;
        }

        public void SetDeleted(bool isDeleted)
        {
            IsDeleted = isDeleted;
            if (isDeleted)
                SetActive(false);
        }
    }
}