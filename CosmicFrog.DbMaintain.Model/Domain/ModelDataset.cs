﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;
using System;
using System.Collections.Generic;

namespace CosmicFrog.DbMaintain.Model.Domain
{
    public class ModelDataset : IAuditableEntity, ISoftDeleteableEntity
    {
        public int Id { get; set; }

        public string Uid { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string OwnerId { get; set; }

        public string OwnerFirstName { get; set; }

        public string OwnerLastName { get; set; }

        public byte[] Image { get; set; }

        public ModelDatasetType ModelDatasetType { get; set; }

        public int ModelDatasetTypeId { get; set; }

        public Folder Folder { get; set; }

        public int?  FolderId { get; set; }

        public List<ModelDatasetTag> Tags { get; set; }

        public int SharedDatasetId { get; set; }

        public List<SharedDataset> SharedDatasets { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public ModelDataset()
        {
            IsActive = true;
            Uid = Guid.NewGuid().ToString();
        }

        public void SetCreated()
        {
            CreatedAt = DateTime.Now;
        }

        public void SetUpdated()
        {
            UpdatedAt = DateTime.Now;
        }

        public void SetActive(bool isActive)
        {
            IsActive = isActive;
        }

        public void SetDeleted(bool isDeleted)
        {
            IsDeleted = isDeleted;
            if (isDeleted)
                SetActive(false);
        }
    }
}