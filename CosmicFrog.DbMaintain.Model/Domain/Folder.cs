﻿using CosmicFrog.DbMaintain.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CosmicFrog.DbMaintain.Model.Domain
{
    public class Folder : IAuditableEntity, ISoftDeleteableEntity
    {
        public int Id { get; set; }

        public string Uid { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int? ParentFolderId { get; set; }

        public string UserId { get; set; }

        public string UserFirstName { get; set; }

        public string UserLastName { get; set; }

        public List<ModelDataset> ModelDatasets { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public Folder()
        {
            IsActive = true;
            Uid = Guid.NewGuid().ToString();
        }

        public void SetCreated()
        {
            CreatedAt = DateTime.Now;
        }

        public void SetUpdated()
        {
            UpdatedAt = DateTime.Now;
        }

        public void SetActive(bool isActive)
        {
            IsActive = isActive;
        }

        public void SetDeleted(bool isDeleted)
        {
            IsDeleted = isDeleted;
            if (isDeleted)
                SetActive(false);
        }
    }
}