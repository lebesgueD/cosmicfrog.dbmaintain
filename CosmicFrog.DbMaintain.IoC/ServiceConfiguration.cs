﻿using AutoMapper;
using CosmicFrog.DbMaintain.Contract;
using CosmicFrog.DbMaintain.Infrastructure.Configuration;
using CosmicFrog.DbMaintain.Infrastructure.HttpControllers;
using CosmicFrog.DbMaintain.Infrastructure.Logging;
using CosmicFrog.DbMaintain.Model.Repositories;
using CosmicFrog.DbMaintain.Repository;
using CosmicFrog.DbMaintain.Service;
using CosmicFrog.DbMaintain.Service.Mapping;
using JustEat.StatsD;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.IdentityModel.Tokens;
using System;

namespace CosmicFrog.DbMaintain.IoC
{
    public static class ServiceConfiguration
    {
        #region Public Methods

        public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            ConfigureLogging(services, configuration);
            ConfigureApi(services, configuration);
            ConfigureApplicationServices(services);
            ConfigureRepositories(services, configuration);
            ConfigureKeycloak(services, configuration);
        }

        #endregion

        #region Private Methods

        private static void ConfigureLogging(IServiceCollection services, IConfiguration configuration)
        {
            LoggerDatabaseSettings swaggerOptions = configuration.GetSection(nameof(LoggerDatabaseSettings)).Get<LoggerDatabaseSettings>();

            LoggerService.ConfigureMsSqlServerLogger(swaggerOptions);
        }

        private static void ConfigureApi(IServiceCollection services, IConfiguration configuration)
        {
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddStatsD(configuration.GetSection(nameof(StatsD)).Get<StatsD>().Server);

            services.AddHealthChecks()
               .AddCheck<ApiHealthCheck>("api")
               .AddCheck("selfTest", () => HealthCheckResult.Healthy())
               .AddSqlServer(configuration.GetSection(nameof(DatabaseConfig)).Get<DatabaseConfig>().ConnectionString,
                   healthQuery: "select 1",
                   name: "sqlServer");

            services.AddHttpContextAccessor();
        }

        private static void ConfigureApplicationServices(IServiceCollection services)
        {
            services.AddTransient<IModelDatasetService, ModelDatasetService>();
            services.AddTransient<IModelDatasetTypeService, ModelDatasetTypeService>();

            var mappingProfile = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new CosmicFrogMappingProfile());
            });

            services.AddSingleton(mappingProfile.CreateMapper());
        }

        private static void ConfigureRepositories(IServiceCollection services, IConfiguration configuration)
        {
            DatabaseConfig databaseConfig = configuration.GetSection(nameof(DatabaseConfig)).Get<DatabaseConfig>();

            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlServer(databaseConfig.ConnectionString)
                .LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
            });

            services.AddTransient<IModelDatasetRepository, ModelDatasetRepository>();
            services.AddTransient<IModelDatasetTypeRepository, ModelDatasetTypeRepository>();
            services.AddTransient<IModelDatasetTagRepository, ModelDatasetTagRepository>();
        }

        private static void ConfigureKeycloak(IServiceCollection services, IConfiguration configuration)
        {
            KeycloakSettings keycloakSettings = configuration.GetSection(nameof(KeycloakSettings)).Get<KeycloakSettings>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = keycloakSettings.RequireHttpsMetadata;
                cfg.Authority = keycloakSettings.Authority;
                cfg.IncludeErrorDetails = keycloakSettings.IncludeErrorDetails;
                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateAudience = keycloakSettings.ValidateAudience,
                    ValidateIssuerSigningKey = keycloakSettings.ValidateIssuerSigningKey,
                    ValidateIssuer = keycloakSettings.ValidateIssuer,
                    ValidIssuer = keycloakSettings.Authority,
                    ValidateLifetime = keycloakSettings.ValidateLifetime
                };

                cfg.Events = new JwtBearerEvents()
                {
                    OnAuthenticationFailed = c =>
                    {
                        c.NoResult();
                        c.Response.StatusCode = 401;
                        c.Response.ContentType = "application/json";
                        return c.Response.WriteAsync(c.Exception.ToString());
                    }
                };
            });
        }

        #endregion
    }
}