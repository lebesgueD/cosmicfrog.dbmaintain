﻿using CosmicFrog.DbMaintain.Contract;
using CosmicFrog.DbMaintain.Infrastructure.HttpControllers;
using CosmicFrog.DbMaintain.Messaging.ModelDataset;
using CosmicFrog.DbMaintain.Messaging.ModelDatasetTag;
using CosmicFrog.DbMaintain.Messaging.Template;
using CosmicFrog.DbMaintain.Messaging.Views;
using JustEat.StatsD;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CosmicFrog.DbMaintain.API.Controllers
{
    [ApiVersion("1")]
    [Route("cosmicfrog/v{version:apiVersion}/dbmaintain/[controller]")]
    [ApiController]
    [Authorize]
    public class ModelDatasetController : ApiControllerBase
    {
        #region Fields

        private readonly IModelDatasetService _modelDatasetService;
        private readonly string MrTemplate = "98d16c9e-88c8-4f65-87c6-620551df7ba7";
        private readonly IStatsDPublisher _statsD;

        #endregion

        #region Constructor

        public ModelDatasetController
            (
            IModelDatasetService modelDatasetService,
            IStatsDPublisher statsPublisher
            )
        {
            _modelDatasetService = modelDatasetService;
            _statsD = statsPublisher;
        }

        #endregion

        #region Public Methods

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<GetTemplatesResponseView>>> GetTemplates()
        {
            GetTemplatesRequest request = CreateServiceRequest<GetTemplatesRequest>();
            request.OwnerId = MrTemplate;

            _statsD.Increment(GetStatsDTag("gettemplates_count", "mr_template"));
            using (_statsD.StartTimer(GetStatsDTag("gettemplates_time", "mr_template")))
            {
                GetTemplatesResponse response = await _modelDatasetService.GetTemplates(request);

                if (response.Success)
                    return Ok(new GetTemplatesResponseView(response));

                return NotFound(new GetTemplatesResponseView(response));
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<GetTemplatesResponseView>>> GetMyModels()
        {
            GetTemplatesRequest request = CreateServiceRequest<GetTemplatesRequest>();
            request.OwnerId = ExtractClaim(ExtractJwt(Request));

            _statsD.Increment(GetStatsDTag("getmymodels_count", request.OwnerId));
            using (_statsD.StartTimer(GetStatsDTag("getmymodels_time", request.OwnerId)))
            {
                GetTemplatesResponse response = await _modelDatasetService.GetTemplates(request);

                if (response.Success)
                    return Ok(new GetTemplatesResponseView(response));

                return NotFound(new GetTemplatesResponseView(response));
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Post([FromBody] ModelDatasetCreateView newModelDataset)
        {
            CreateModelDatasetRequest request = CreateServiceRequest<CreateModelDatasetRequest>();
            request.NewModelDataset = newModelDataset;
            request.OwnerId = ExtractClaim(ExtractJwt(Request));

            _statsD.Increment(GetStatsDTag("post_count", request.OwnerId));
            using (_statsD.StartTimer(GetStatsDTag("post_time", request.OwnerId)))
            {
                CreateModelDatasetResponse response = await _modelDatasetService.CreateDataset(request);

                if (response.Success)
                    return Ok(response.Message);

                return BadRequest(response.Message);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> Put([FromBody] ModelDatasetUpdateView dataset)
        {
            UpdateModelDatasetRequest request = CreateServiceRequest<UpdateModelDatasetRequest>();
            request.ModelDataset = dataset;
            request.OwnerId = ExtractClaim(ExtractJwt(Request));

            _statsD.Increment(GetStatsDTag("put_count", request.OwnerId));
            using (_statsD.StartTimer(GetStatsDTag("put_time", request.OwnerId)))
            {
                UpdateModelDatasetResponse response = await _modelDatasetService.UpdateDataset(request);

                if (response.Success)
                    return Ok(response.Message);

                return BadRequest(response.Message);
            }
        }

        [HttpDelete("{modelId}")]
        public async Task<IActionResult> Delete(int modelId)
        {
            DeleteModelDatasetRequest request = CreateServiceRequest<DeleteModelDatasetRequest>();
            request.ModelId = modelId;
            request.OwnerId = ExtractClaim(ExtractJwt(Request));

            _statsD.Increment(GetStatsDTag("delete_count", request.OwnerId));
            using (_statsD.StartTimer(GetStatsDTag("delete_time", request.OwnerId)))
            {
                DeleteModelDatasetResponse response = await _modelDatasetService.DeleteDataset(request);

                if (response.Success)
                    return Ok(response.Message);

                return NotFound(response.Message);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<GetModelDatasetTagsResponseView>> GetModelDatasetTags(int modelId)
        {
            GetModelDatasetTagsRequest request = CreateServiceRequest<GetModelDatasetTagsRequest>();
            request.ModelId = modelId;
            request.OwnerId = ExtractClaim(ExtractJwt(Request));

            _statsD.Increment(GetStatsDTag("getmodeldatasettags_count", request.OwnerId));
            using (_statsD.StartTimer(GetStatsDTag("getmodeldatasettags_time", request.OwnerId)))
            {
                GetModelDatasetTagsResponse response = await _modelDatasetService.GetModelDatasetTags(request);

                if (response.Success)
                    return Ok(new GetModelDatasetTagsResponseView(response));

                return NotFound(new GetModelDatasetTagsResponseView(response));
            }
        }

        #endregion

        #region Private Methods

        private static string GetStatsDTag(string entryPoint, string ownerId)
        {
            string tag = "cosmicfrog.dbmaintain";

            return $"{tag}.{entryPoint},ownerId={ownerId}";
        }

        #endregion
    }
}